import warnings
warnings.filterwarnings("ignore")



import dash,cv2,numpy as np,pandas as pd,skvideo.io,os,random,flask
from dash.dependencies import Input, Output, State
from dash import dcc,html
from base64 import decodebytes
from jeanCV import skinDetector
from functions import *
from dash_extensions.enrich import Trigger, FileSystemCache
import dash_bootstrap_components as dbc

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

fsc = FileSystemCache("cache_dir",default_timeout=0)
fsc.set("progress", None)

#server = flask.Flask(__name__)

app = dash.Dash(__name__, external_stylesheets=[dbc.themes.BOOTSTRAP], suppress_callback_exceptions=True, prevent_initial_callbacks=True)

server = app.server

@app.callback(
    Output("result", "children"), 
    Trigger("result_interval", "n_intervals"), 
    prevent_initial_call=True)
def update_progress(t):
    value = fsc.get("progress")
    if (value is None):
        return "";
    else:
        return value


@app.callback(
    Output('result2', 'children'),
    [
    Input('init', 'n_clicks'),
    State('urlvideo', 'value'),
    ],prevent_initial_call=True
)
def init_prediction(n_clicks,urlvideo):
    fsc.set("progress", None)
    temp_path = "data_cache"
    #nombre_archivo = str(random.randint(0,100000))+"-"+str(random.randint(0,10000))+".mp4"
    #if contents == None or contents == "":
    #    mensaje = html.H4("Por favor, ingrese un video primero", className="text-danger")
    #    fsc.set("progress", mensaje)
    #    return None
    #try:
    #    data = contents.encode("utf8").split(b";base64,")[1]
    #    with open(temp_path+'/'+nombre_archivo,'wb') as wfile:
    #        wfile.write(decodebytes(data))
    #except:
    #    mensaje = html.H4("Por favor, ingrese un video de menor tamaño", className="text-danger")
    #    fsc.set("progress", mensaje)
    #    return None

    cap = []
    vcap = cv2.VideoCapture(urlvideo)

    while(True):
        ret, frame = vcap.read()
        if frame is not None:
            cap.append(frame)
        else:
            break

    if len(cap) == 0:
        fsc.set("progress", html.H4("Lo siento, no pudimos leer el video", className="text-danger"))
        return None

    #try:
    #    cap = skvideo.io.vread(temp_path+'/'+nombre_archivo)
    #except:
    #    mensaje = html.H4("Por favor, ingrese un video de menor tamaño", className="text-danger")
    #    fsc.set("progress", mensaje)
    #    return None
    #if os.path.exists(temp_path+'/'+nombre_archivo):
    #    os.remove(temp_path+'/'+nombre_archivo)

    column_min_capa_verde = []
    column_max_capa_verde = []
    column_mean_capa_verde = []
    column_std_capa_verde = []

    faceCascade = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt0.xml")
    faceCascade_profileface = cv2.CascadeClassifier("haarcascades/haarcascade_profileface.xml")
    faceCascade_default = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_default.xml")
    faceCascade1 = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt1.xml")
    faceCascade2 = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt2.xml")

    alphas = [1]
    betas = [100]
    scale_factor = 1.3
    minneighbours = 5

    len_total = len(cap)
    index = 0



    for img in cap:

        per = int((index/len_total)*100)
        fsc.set("progress", "Progress: "+str(per)+" %")

        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        roi_frame = img
        face_rects = faceCascade.detectMultiScale(gray, scale_factor, minneighbours)

        if len(face_rects) != 1:
            for a in alphas:
                for b in betas:
                    img_transformada = cv2.convertScaleAbs(img, alpha=a, beta=b)
                    gray = cv2.cvtColor(img_transformada, cv2.COLOR_RGB2GRAY)
                    face_rects = faceCascade.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;
                    face_rects = faceCascade_default.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;
                    face_rects = faceCascade1.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;
                    face_rects = faceCascade2.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;


        if len(face_rects) == 1:

            for (x, y, w, h) in face_rects:
                roi_frame = img[y:y + h, x:x + w]
            detector = skinDetector(cv2.cvtColor(np.array(roi_frame), cv2.COLOR_BGR2RGB))
            roi_frame = detector.find_skin()
            roi_frame = roi_frame[:,:,:]
            frame = np.ndarray(shape=roi_frame.shape, dtype="float")
            frame[:] = roi_frame * (1./255)
            capa_verde = np.zeros((frame.shape[0],frame.shape[1],1))
            capa_verde[:,:,0] = frame[:,:,1]
            capa_verde = capa_verde.reshape(frame.shape[0]*frame.shape[1])
            del frame;
            pixeles = []
            for a in capa_verde:
                if a > 0:
                  pixeles.append(a)
            min = np.min(pixeles)*255
            max = np.max(pixeles)*255
            mean = np.mean(pixeles)*255
            std = np.std(pixeles)*255

            column_min_capa_verde.append(min)
            column_max_capa_verde.append(max)
            column_mean_capa_verde.append(mean)
            column_std_capa_verde.append(std)

        index += 1

    if len(column_min_capa_verde) == 0:
        fsc.set("progress", html.H4("Lo siento, no pudimos detectar un rostro", className="text-danger"))
        return None

    df_res = pd.DataFrame()
    df_res["min_capa_verde"] = pd.Series(column_min_capa_verde)
    df_res["max_capa_verde"] = pd.Series(column_max_capa_verde)
    df_res["mean_capa_verde"] = pd.Series(column_mean_capa_verde)
    df_res["std_capa_verde"] = pd.Series(column_std_capa_verde)    

    min, max, mean, std, pred = predecir_senial_hr(df_res)

    combinations = []
    combinations.append(html.Br())
    combinations.append("Min HR: "+str(round(min,2)))
    combinations.append(html.Br())
    combinations.append("Max HR: "+str(round(max,2)))
    combinations.append(html.Br())
    combinations.append("Mean HR: "+str(round(mean,2)))
    combinations.append(html.Br())
    combinations.append("Standard deviation HR: "+str(round(std,2)))
    combinations.append(html.Br())

    fsc.set("progress", combinations)
    return None





UPLOAD_FOLDER_ROOT = "/uploads"
SIDEBAR_STYLE = {
    "position": "fixed",
    "top": 0,
    "left": 0,
    "bottom": 0,
    "width": "16rem",
    "padding": "2rem 1rem",
    "background-color": "#f8f9fa",
}
# padding for the page content
CONTENT_STYLE = {
    "margin-left": "18rem",
    "margin-right": "2rem",
    "padding": "2rem 1rem",
}
sidebar = html.Div(
    [
        html.H2("Menu", className="display-4"),
        html.Hr(),
        html.P(
            "", className="lead"
        ),
        dbc.Nav(
            [
                dbc.NavLink("Prediction", href="/", active="exact"),
            ],
            vertical=True,
            pills=True,
        ),
    ],
    style=SIDEBAR_STYLE,
)
content = html.Div(id="page-content", children=[], style=CONTENT_STYLE)
app.layout = html.Div([
    dcc.Location(id="url"),
    sidebar,
    content
])



@app.callback(
    Output("page-content", "children"),
    [Input("url", "pathname"),Input("url", "search"),Input("url", "href")]
)
def render_page_content(pathname,search,href):
    fsc.set("progress", None)
    return html.Div([
        html.Div(id="result2"),
        #html.Br(),
        #dcc.Upload([
        #    'Drag and Drop or ',
        #    html.A('Select a File')
        #], style={
        #    'width': '100%',
        #    'height': '60px',
        #    'lineHeight': '60px',
        #    'borderWidth': '1px',
        #    'borderStyle': 'dashed',
        #    'borderRadius': '5px',
        #    'textAlign': 'center'
        #}, id="input_file", multiple = False
        #),

        html.Br(),
        html.Br(),
        dcc.Input(id="urlvideo", type="text", placeholder="URL Video", style={'width': '100%','textAlign': 'center'}, value="https://medjoinstorage.blob.core.windows.net/external/Generic/Generic_-2bfdb562-fef4-4ca2-96ee-e3e4703d75d4"),
        html.Br(),
        html.Br(),
        html.Center(html.Button("Iniciar",id="init",className='btn btn-success')),
        html.Br(),
        html.Br(),
        html.Center(html.Div(id="result")),
        dcc.Interval(id="result_interval", interval=500)
    ])

#if __name__ == '__main__':
#    app.run_server(debug=True, host='0.0.0.0')