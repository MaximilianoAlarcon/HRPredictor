from dash.dependencies import Input, Output, State
import cv2
import numpy as np
import dash
from dash import dcc
from dash import html
from base64 import decodebytes
from jeanCV import skinDetector
import skvideo.io
import pandas as pd
from functions import *
from dash_extensions.enrich import Trigger, FileSystemCache
import os
import random

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

fsc = FileSystemCache("cache_dir",default_timeout=0)
fsc.set("progress", None)

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

@app.callback(
    Output("result", "children"), 
    Trigger("result_interval", "n_intervals"), 
    prevent_initial_call=True)
def update_progress(t):
    value = fsc.get("progress")
    if (value is None):
        return "";
    else:
        return value


@app.callback(
    Output('result2', 'children'),
    [
    Input('init', 'n_clicks'),
    State('input_file', 'filename'),
    State('input_file', 'contents')
    ],prevent_initial_call=True
)
def init_prediction(n_clicks,filename,contents):
    fsc.set("progress", None)
    temp_path = "data_cache"
    nombre_archivo = str(random.randint(0,100000))+"-"+str(random.randint(0,10000))+".mp4"

    if contents == None or contents == "":
        mensaje = html.H4("Por favor, ingrese un video primero", className="text-danger")
        fsc.set("progress", mensaje)
        return None

    try:
        data = contents.encode("utf8").split(b";base64,")[1]
        with open(temp_path+'/'+nombre_archivo,'wb') as wfile:
            wfile.write(decodebytes(data))
    except:
        mensaje = html.H4("Por favor, ingrese un video de menor tamaño", className="text-danger")
        fsc.set("progress", mensaje)
        return None

    cap = []

    try:
        cap = skvideo.io.vread(temp_path+'/'+nombre_archivo)
    except:
        mensaje = html.H4("Por favor, ingrese un video de menor tamaño", className="text-danger")
        fsc.set("progress", mensaje)
        return None

    if os.path.exists(temp_path+'/'+nombre_archivo):
        os.remove(temp_path+'/'+nombre_archivo)

    column_min_capa_verde = []
    column_max_capa_verde = []
    column_mean_capa_verde = []
    column_std_capa_verde = []

    faceCascade = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt0.xml")
    faceCascade_profileface = cv2.CascadeClassifier("haarcascades/haarcascade_profileface.xml")
    faceCascade_default = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_default.xml")
    faceCascade1 = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt1.xml")
    faceCascade2 = cv2.CascadeClassifier("haarcascades/haarcascade_frontalface_alt2.xml")

    alphas = [1]
    betas = [100]
    scale_factor = 1.15
    minneighbours = 2

    len_total = cap.shape[0]
    index = 0

    for img in cap:

        per = int((index/len_total)*100)
        fsc.set("progress", "Progress: "+str(per)+" %")

        gray = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
        roi_frame = img
        face_rects = faceCascade.detectMultiScale(gray, scale_factor, minneighbours)

        if len(face_rects) != 1:
            for a in alphas:
                for b in betas:
                    img_transformada = cv2.convertScaleAbs(img, alpha=a, beta=b)
                    gray = cv2.cvtColor(img_transformada, cv2.COLOR_RGB2GRAY)
                    face_rects = faceCascade.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;
                    face_rects = faceCascade_default.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;
                    face_rects = faceCascade1.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;
                    face_rects = faceCascade2.detectMultiScale(gray, scale_factor, minneighbours) 
                    if len(face_rects) == 1:
                      break;


        if len(face_rects) == 1:

            for (x, y, w, h) in face_rects:
                roi_frame = img[y:y + h, x:x + w]
            detector = skinDetector(cv2.cvtColor(np.array(roi_frame), cv2.COLOR_BGR2RGB))
            roi_frame = detector.find_skin()
            roi_frame = roi_frame[:,:,:]
            frame = np.ndarray(shape=roi_frame.shape, dtype="float")
            frame[:] = roi_frame * (1./255)
            capa_verde = np.zeros((frame.shape[0],frame.shape[1],1))
            capa_verde[:,:,0] = frame[:,:,1]
            capa_verde = capa_verde.reshape(frame.shape[0]*frame.shape[1])
            del frame;
            pixeles = []
            for a in capa_verde:
                if a > 0:
                  pixeles.append(a)
            min = np.min(pixeles)*255
            max = np.max(pixeles)*255
            mean = np.mean(pixeles)*255
            std = np.std(pixeles)*255

            column_min_capa_verde.append(min)
            column_max_capa_verde.append(max)
            column_mean_capa_verde.append(mean)
            column_std_capa_verde.append(std)

        index += 1

    if len(column_min_capa_verde) == 0:
        fsc.set("progress", html.H4("Lo siento, no pudimos detectar un rostro", className="text-danger"))
        return None

    df_res = pd.DataFrame()
    df_res["min_capa_verde"] = pd.Series(column_min_capa_verde)
    df_res["max_capa_verde"] = pd.Series(column_max_capa_verde)
    df_res["mean_capa_verde"] = pd.Series(column_mean_capa_verde)
    df_res["std_capa_verde"] = pd.Series(column_std_capa_verde)    

    min, max, mean, std, pred = predecir_senial_hr(df_res)

    combinations = []
    combinations.append(html.Br())
    combinations.append("Min HR: "+str(round(min,2)))
    combinations.append(html.Br())
    combinations.append("Max HR: "+str(round(max,2)))
    combinations.append(html.Br())
    combinations.append("Mean HR: "+str(round(mean,2)))
    combinations.append(html.Br())
    combinations.append("Standard deviation HR: "+str(round(std,2)))
    combinations.append(html.Br())

    fsc.set("progress", combinations)

app.layout = html.Div([
    html.Div(id="result2"),
    html.Br(),
    dcc.Upload([
        'Drag and Drop or ',
        html.A('Select a File')
    ], style={
        'width': '100%',
        'height': '60px',
        'lineHeight': '60px',
        'borderWidth': '1px',
        'borderStyle': 'dashed',
        'borderRadius': '5px',
        'textAlign': 'center'
    }, id="input_file", multiple = False
    ),
    html.Br(),
    html.Center(html.Button("Iniciar",id="init")),
    html.Br(),
    html.Center(html.Div(id="result")),
    dcc.Interval(id="result_interval", interval=500)
])

if __name__ == '__main__':
    app.run_server(host="0.0.0.0", port="8050",debug=False,dev_tools_ui=False)